package com.arraypairs;

import java.util.*;

public class ArrayPairs {

    public static Set pairArrayBasedOnSum(int[] integers, int sum) {
        if (integers.length < 2) {
            throw new IllegalArgumentException("Array must have at least two items.");
        }
        if (sum <= 0) {
            throw new IllegalArgumentException("Sum must be greater than zero.");
        }
        Set<Integer> sumPair = new HashSet<>();
        LinkedHashSet<LinkedHashSet<Integer> > pairs
                = new LinkedHashSet<LinkedHashSet<Integer> >();

        for (int i = 0; i < integers.length; i++) {

            int temp = sum - integers[i];

            if (sumPair.contains(temp)) {
                System.out.println("Pair with given sum " + sum + " is (" + integers[i] + ", " + temp + ")");
                pairs.add(new LinkedHashSet<Integer>(Arrays.asList(integers[i],temp)));
            }
            sumPair.add(integers[i]);
        }
        return pairs;
    }
    public static void main(String[] args) {
        int numbers[] = new int[] {1, 2, 3, 4, 5, 6};
        Set pairs= ArrayPairs.pairArrayBasedOnSum(numbers, 7);
        System.out.println("Generated Pairs based on sum: " + pairs);
    }
}