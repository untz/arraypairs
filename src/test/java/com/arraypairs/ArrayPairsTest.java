package com.arraypairs;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class ArrayPairsTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void pairArrayBasedOnSum_whenSumIs7() {
        int[] integers = new int[]{1, 2, 3, 4, 5, 6};
        Set expected = ArrayPairs.pairArrayBasedOnSum(integers, 7);

        LinkedHashSet<LinkedHashSet<Integer>> pairs
                = new LinkedHashSet<LinkedHashSet<Integer>>();

        pairs.add(new LinkedHashSet<Integer>(Arrays.asList(4,3)));
        pairs.add(new LinkedHashSet<Integer>(Arrays.asList(5,2)));
        pairs.add(new LinkedHashSet<Integer>(Arrays.asList(6,1)));

        assertEquals("The pairs summed up are incorrect.", expected, pairs);
    }

    @Test
    public void pairArrayBasedOnSum_whenArrayIsLessThanTwoItems() {
        int[] integer = new int[]{1};
        exception.expect(IllegalArgumentException.class);
        Set expected = ArrayPairs.pairArrayBasedOnSum(integer, 7);
    }

    @Test
    public void pairArrayBasedOnSum_whenSumIsZero() {
        int[] integers = new int[]{1, 2, 3, 4, 5, 6};
        exception.expect(IllegalArgumentException.class);
        Set expected = ArrayPairs.pairArrayBasedOnSum(integers, 0);
    }

}