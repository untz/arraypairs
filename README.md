# Find Pairs in Array Based On Sum

## Specification

Write a program that accepts an input of an array of integers and returns the distinct pairs of these integers that sum to a specific number.

For example, if sum is ```7``` and the input is ```[1, 2, 3, 4, 5, 6]```, the output could be ```(1,6), (2, 5), (3, 4)```, or ```(6,1), (5, 2), (4,3)```. 

Different combinations of the same pair of numbers e.g, ```(1, 6)``` and ```(6, 1)``` are not distinct. 

In this case, you should only return ```(1, 6)``` or ```(6, 1)``` but not both of them.

---

### Acceptance Criteria

* Must return correct paired values based on sum.

* Improper parameter inputs (array must have at least two items & sum must not be zero)
must throw specific IllegalArgumentExceptions.

* Must be testable via JUnit (full code coverage, especially the IllegalArgumentExceptions) and run via a Maven build script.

---

### System Requirements

Need a computer with Java 1.8 (whether its the JDK or JRE) installed along with Apache Maven 3.3.3 set to run from the command line.

---

#### How to build and run the project

First way, is directly from command line, go to the root of the project and type:

`mvn clean install` (this runs the unit tests and will print out the results to stdout).